#Overview

This files reports the performance evaluation results of the implementation
of the extensible model for continuous authorisation.

The evaluation was conducted on a machine running Ubuntu 21.04LTS with
core i7-9850H CPU. The implementation is optimised for embedded systems
and completely written in C++.

The model decouples the authorisation lifecycle from policy evaluation. It enables
expressing the authorisation lifecycle as a Deterministic Finite Automata (DFA) where
state represent the different phases of the lifecycle. The model also classifies
access policies and rules by the different phases of the lifecycle, such that at each
phase only the corresponding policies are evaluated. This makes policy evaluation
identical for all lifecycles. For this reason, we evaluated the overhead of policy
evaluation and the overhead of lifecycles separately.

Results are reported below:

### Overhead of policy evaluation (evaluation function)
We measured the overhead 
of policy evaluation with different numbers of attributes (number of 
attributes involved in the policy) to study the effect of the number of 
attributes of the performance of the evaluation function.
We specifically measured the time to evaluate 4 policies that rely on 2, 4, 8 and
16 attributes respectively. We ran each test 
1000 rounds and computed the mean, median, standard deviation, and 
confidence interval. The results are shown in the table below.

| Number of Attributes                               | 2            | 4            | 8              | 16             |
|----------------------------------------------------|--------------|--------------|----------------|----------------|
| Mean (μs)                                          | 67.3         | 87.2         | 156.4          | 365.0          |
| Standard Deviation (μs)                            | 14.1         | 22.7         | 23.0           | 49.1           |
| Confidence Interval (μs)<br/>Confidence Level: 95% | [66.6, 67.9] | [86.2, 88.2] | [155.4, 157.4] | [362.8, 367.1] |

### Overhead of authorisation lifecycle (transition function)
We evaluated the lifecycle overhead of 3 instantiations of the model:
traditional ABAC, traditional UCON, and extended UCON (which we introduced
in the paper). For each instantiation, we measured the time
required to enact the lifecycle and enact
transitions between phases in the automata (transition function).
We ran each test 
1000 rounds and computed the mean, median, standard deviation, and 
confidence interval. The results are shown in the table below.

| Lifecycle                                          | ABAC         | UCON         | Extended UCON |
|----------------------------------------------------|--------------|--------------|---------------|
| Mean (μs)                                          | 30.6         | 40.7         | 45.6          |
| Standard Deviation (μs)                            | 4.2          | 10.7         | 15.2          |
| Confidence Interval (μs)<br/>Confidence Level: 95% | [30.4, 30.8] | [40.1, 41.3] | [44.6, 46.5]  |
